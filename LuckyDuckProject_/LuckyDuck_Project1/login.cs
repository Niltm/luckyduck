﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using System.Runtime.InteropServices; 

namespace LuckyDuck_Project1
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        Class1 acceso = new Class1();
        //------------ Cerrar programa con X -----------------------------------------------------------------------
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        //------------ Validacion Usuario --------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            string consulta = "Select *  from users" + " where CodeUser = '" + textBox1.Text + "' and Password ='" + textBox2.Text + "';";
           // string name = ;
            int registres = acceso.PortarPerConsulta(consulta).Tables[0].Rows.Count;

            if (registres > 0)
            {
                string usuario = textBox1.Text;
                this.Hide();
                welcome frm4 = new welcome(usuario);
                frm4.Show();
            }
        }

        public static class ClaseCompartida
        {
            public static string usuario;
            // otras variables estáticas
        }



        /////--------- Text Box Usuario y Contraseña ---------------------------------------------------------------
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Opacity = .98;
            textBox1.Text = "User";
            textBox1.ForeColor = Color.Gray;
            textBox2.Text = "Password";
            textBox2.ForeColor = Color.Gray;

        }
        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "User")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
            }
        }
        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "User";
                textBox1.ForeColor = Color.Gray;
            }
        }
        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "Password")
            {
                textBox2.Text = "";
                textBox2.ForeColor = Color.Black;
                textBox2.UseSystemPasswordChar = true;
            }
        }
        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.Text = "Password";
                textBox2.ForeColor = Color.Gray;
                textBox2.UseSystemPasswordChar = false;
            }
        }

        private void login_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);

        }
    }
}
