﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml.Schema;
using AccesoDatos;
using System.Data;

namespace Controls03
{
    public class SWTextbox : TextBox
    {
        public SWTextbox()
        {
            InitializeComponent();
        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SWTextbox
            // 
            this.TextChanged += new System.EventHandler(this.SWTextbox_TextChanged);
            this.Enter += new System.EventHandler(this.SWTextbox_Enter);
            this.Leave += new System.EventHandler(this.SWTextbox_Leave);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.SWTextbox_Validating);
            this.ResumeLayout(false);

        }
        public enum TipusDada
        {
            Numero,
            Text,
            Codi,
            Fecha
        }
        private TipusDada _DadaPermesa;
        public TipusDada DadaPermesa
        {
            get { return _DadaPermesa; }
            set
            {
                _DadaPermesa = value;
            }
        }
        private bool _obligat;
        public bool Obligat
        {
            get { return _obligat; }
            set { _obligat = value; }
        }

        private void SWTextbox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Regex Num = new Regex("^[0-9]+$");
            Regex Text = new Regex("^[a-zA-Z]+$");
            Regex Data = new Regex("[0-9][0-9]/[0-9][0-9]/[0-9][0-9]");
            Regex Codigo = new Regex("[A-Z][A-Z][A-Z][A-Z]-[0-9][0-9][0-9]/[13579][AEIOU]");

            switch (this.DadaPermesa)
            {
                case TipusDada.Numero:
                    if (!Num.IsMatch(this.Text) && Obligat == true)
                    {
                        e.Cancel = true;
                    }
                    else if (!Num.IsMatch(this.Text))
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                    break;
                case TipusDada.Text:
                    if (!Text.IsMatch(this.Text) && Obligat == true)
                    {
                        e.Cancel = true;
                    }
                    else if (!Text.IsMatch(this.Text))
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                    break;
                case TipusDada.Fecha:
                    if (!Data.IsMatch(this.Text) && Obligat == true)
                    {
                        e.Cancel = true;
                    }
                    else if (!Data.IsMatch(this.Text))
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                    break;
                case TipusDada.Codi:
                    if (!Codigo.IsMatch(this.Text) && Obligat == true)
                    {
                        e.Cancel = true;
                    }
                    else if (!Num.IsMatch(this.Text))
                    {
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                    break;
            }
        }

        //private string _ControlId;
        //public string ControlId
        //{
        //    get { return this._ControlId; }
        //    set
        //    {
        //        this._ControlId = value;
        //    }
        //}

        private void SWTextbox_Enter(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(204, 230, 255);
        }

        private void SWTextbox_Leave(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.White;

        }
        Class1 acceso = new Class1();

        private void SWTextbox_TextChanged(object sender, EventArgs e)
        {
            //Form form = this.FindForm();
            //foreach (Control ctr in form.Controls)
            //{
            //    if (ControlId == ctr.Name)
            //    {
            //        SWComboFK combo = (SWComboFK)ctr;
            //        combo.SelectedValue = this.Text;
            //    }
            //}
        }
    }
}
