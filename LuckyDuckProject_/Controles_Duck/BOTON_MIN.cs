﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Controls03
{
    public class BOTON_MIN : Button
    {
        public BOTON_MIN()
        {
            InitializeComponent();
        }
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BOTON_MIN));
            this.SuspendLayout();
            // 
            // BOTON_MIN
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FlatAppearance.BorderSize = 0;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Size = new System.Drawing.Size(20, 20);
            this.UseVisualStyleBackColor = false;
            this.Click += new System.EventHandler(this.BOTON_MIN_Click);
            this.ResumeLayout(false);

        }

        public void BOTON_MIN_Click(object sender, EventArgs e)
        {
            Form frm = this.FindForm();
            frm.WindowState = FormWindowState.Minimized;
        }
    }

}
