﻿namespace Formularios
{
    partial class Frm_Base
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Base));
            this.panelB = new System.Windows.Forms.Panel();
            this.botoN_X1 = new Controls03.BOTON_X();
            this.botoN_MIN1 = new Controls03.BOTON_MIN();
            this.panelB.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelB
            // 
            this.panelB.BackgroundImage = global::Formularios.Properties.Resources.banner1;
            this.panelB.Controls.Add(this.botoN_X1);
            this.panelB.Controls.Add(this.botoN_MIN1);
            this.panelB.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelB.ImeMode = System.Windows.Forms.ImeMode.On;
            this.panelB.Location = new System.Drawing.Point(0, 0);
            this.panelB.Name = "panelB";
            this.panelB.Size = new System.Drawing.Size(1809, 220);
            this.panelB.TabIndex = 0;
            // 
            // botoN_X1
            // 
            this.botoN_X1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.botoN_X1.BackColor = System.Drawing.Color.Transparent;
            this.botoN_X1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botoN_X1.BackgroundImage")));
            this.botoN_X1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botoN_X1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.botoN_X1.FlatAppearance.BorderSize = 0;
            this.botoN_X1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botoN_X1.Location = new System.Drawing.Point(1731, 36);
            this.botoN_X1.Name = "botoN_X1";
            this.botoN_X1.Size = new System.Drawing.Size(37, 35);
            this.botoN_X1.TabIndex = 1;
            this.botoN_X1.Text = " ";
            this.botoN_X1.UseVisualStyleBackColor = false;
            // 
            // botoN_MIN1
            // 
            this.botoN_MIN1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.botoN_MIN1.BackColor = System.Drawing.Color.Transparent;
            this.botoN_MIN1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botoN_MIN1.BackgroundImage")));
            this.botoN_MIN1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botoN_MIN1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.botoN_MIN1.FlatAppearance.BorderSize = 0;
            this.botoN_MIN1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botoN_MIN1.Location = new System.Drawing.Point(1688, 36);
            this.botoN_MIN1.Name = "botoN_MIN1";
            this.botoN_MIN1.Size = new System.Drawing.Size(37, 35);
            this.botoN_MIN1.TabIndex = 0;
            this.botoN_MIN1.UseVisualStyleBackColor = false;
            // 
            // Frm_Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Formularios.Properties.Resources.daa6550f1247a73508c5560300d9d697;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1809, 859);
            this.Controls.Add(this.panelB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Base";
            this.panelB.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel panelB;
        public Controls03.BOTON_X botoN_X1;
        public Controls03.BOTON_MIN botoN_MIN1;
    }
}
